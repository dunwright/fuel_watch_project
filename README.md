### FUEL WATCH PROJECT INFORMATION

## About

A web page html generator that can take filters as **kwargs for producing a web page that can be run locally in your browser.

The data is sourced from https://www.fuelwatch.wa.gov.au

If filters are supplied,  a URL Constructor generates a url with Fuel Watch RSS compatible filters to reduce the size of the dat
a returned

### Useage

Using your preferred method copy the repository onto you computer.

Install dependencies using requirements.txt

Navigate to the folder


1. ***CLI***
> python fuel_watch_frontend.py
>> This will generate the html file with default filters
>
2. ***CLI***
> \>\>\> import fuel_watch_frontend as fwf
>> \>\>\>fwf.generate_fuel_watch_html(filtered=True, \*\*kwargs=\{"Surrounding": "yes", "Product": "Unleaded Petrol", "Sort_on": "price"\}
>> *This returns the default data set*
>>>a. **filtered:** 
>>>
    >>> i. *True:* A filtered data set is sent to html_generator, the data is fitlered using keys_of_interest\[\] 
>>>    
    >>> ii. *False:* A full set of data is sent to the html_generator, most of this is of little use to us
>>>
>>>b. **\*\*kwargs: -** showing default settings
    >>>
                >>>>\"Region\": ' '
>>>
            >>>> "Suburb": ' '
>>>
            >>>>  "Product": 'Unleaded Petrol'
>>>
            >>>>  "Day": ' '
    >>>
            >>>>  "Surrounding": 'yes'
>>>    
            >>>>  "Brand": ' '
>>>
            >>>>  "Sort_on":' '
>>>
            >>>>  "keys_of_interest": \[ \]

2. ***Jupyter Notebook***
> See notes below


## Jupyter Notebook

#### *kwargs values can be changed using filter options described below*

Fuel Watch Project is the Jupyter Notebook on this folder set up ready to go.  It includes all the information that is in this README


```python
import fuel_watch_frontend as fwf

print('Generating file')
kwargs = {"Region": '',
          "Suburb": 'Armadale',
          "Product": 'Unleaded Petrol',
          "Day": '',
          "Surrounding": 'yes',
          "Brand": '',
          "Sort_on": 'price',
          "keys_of_interest": [
              'updated',
              'price',
              'trading-name',
              'brand',
              'address',
              'location',
          ],
          }

fwf.generate_fuel_watch_html(filtered=True, **kwargs)
print('Fuel Watch.html has been generated and can be found in \html_files')
```

# Filters

#### Run the following cell to see a list of all the available keys and filter options.  FilterName is not an option its a tag name...


```python
import fuel_watch_filters as filters

VALID_URL_FILTERS = {
        "Brand": filters.Brand(),
        "Day": filters.Day(),
        "Product": filters.Product(),
        "Region": filters.Region(),
        "StateRegion": filters.StateRegion(),
        "Suburb": filters.Suburb(),
        "Surrounding": filters.Surrounding(),
        "Sort_on": filters.Sort_on(),
        "keys_of_interest": filters.kwargs_keys()
    
    }


for key, value in  VALID_URL_FILTERS.items():
    print('***  ',key, ' Filters are   ***')
    print('\n')
    print(VALID_URL_FILTERS[key].keys())
    print('\n')
```

    ***   Brand  Filters are   ***
    
    
    dict_keys(['FilterName', '7-Eleven', 'BOC', 'BP', 'Better Choice', 'Caltex', 'Caltex Woolworths', 'Coles Express', 'Eagle', 'FastFuel 24/7', 'Gull', 'Independent', 'Kleenheat', 'Kwikfuel', 'Liberty', 'Metro Petroleum', 'Mobil', 'Peak', 'Puma', 'Shell', 'United', 'Vibe', 'WA Fuels', 'Wesco'])
    
    
    ***   Day  Filters are   ***
    
    
    dict_keys(['FilterName', 'today', 'today_tomorrow', 'tomorrow', 'yesterday'])
    
    
    ***   Product  Filters are   ***
    
    
    dict_keys(['FilterName', 'Unleaded Petrol', 'Premium Unleaded', 'Diesel', 'LPG', '98 RON', 'E85', 'Brand diesel'])
    
    
    ***   Region  Filters are   ***
    
    
    dict_keys(['FilterName', 'Albany', 'Augusta/Margaret River', 'Boulder', 'Bridgetown/Greenbushes', 'Broome', 'Bunbury', 'Busselton (Shire)', 'Busselton (Townsite)', 'Capel', 'Carnarvon', 'Cataby', 'Collie', 'Coolgardie', 'Cunderdin', 'Dalwallinu', 'Dampier', 'Dardanup', 'Denmark', 'Derby', 'Dongara', 'Donnybrook/Balingup', 'Esperance', 'Exmouth', 'Fitzroy Crossing', 'Geraldton', 'Greenough', 'Harvey', 'Jurien', 'Kalgoorlie', 'Kambalda', 'Karratha', 'Kellerberrin', 'Kojonup', 'Kununurra', 'Mandurah', 'Manjimup', 'Meckering', 'Meekatharra', 'Metro : East/Hills', 'Metro : North of River', 'Metro : South of River', 'Moora', 'Mt Barker', 'Munglinup', 'Murray', 'Narrogin', 'Newman', 'Norseman', 'North Bannister', 'Northam', 'Port Hedland', 'Ravensthorpe', 'Regans Ford', 'South Hedland', 'Tammin', 'Waroona', 'Williams', 'Wubin', 'Wundowie', 'York'])
    
    
    ***   StateRegion  Filters are   ***
    
    
    dict_keys(['FilterName', 'Gascoyne', 'Goldfields-Esperance', 'Great Southern', 'Kimberley', 'Metro', 'Mid-West', 'Peel', 'Pilbara', 'South-West', 'Wheatbelt'])
    
    
    ***   Suburb  Filters are   ***
    
    
    dict_keys(['Filtername', 'Albany', 'Alexander Heights', 'Alfred Cove', 'Alkimos', 'Applecross', 'Armadale', 'Ascot', 'Ashby', 'Attadale', 'Augusta', 'Australind', 'Aveley', 'Balcatta', 'Baldivis', 'Balga', 'Balingup', 'Ballajura', 'Banksia Grove', 'Barragup', 'Baskerville', 'Bassendean', 'Bayswater', 'Beckenham', 'Bedfordale', 'Beechboro', 'Beeliar', 'Beldon', 'Bellevue', 'Belmont', 'Bentley', 'Bertram', 'Bibra Lake', 'Bicton', 'Binningup', 'Boulder', 'Boyanup', 'Brentwood', 'Bridgetown', 'Broadwater', 'Broome', 'Brunswick', 'Bull Creek', 'Bullsbrook', 'Bunbury', 'Burswood', 'Busselton', 'Butler', 'Byford', 'Canning Vale', 'Cannington', 'Capel', 'Carbunup River', 'Carine', 'Carlisle', 'Carnarvon', 'Cataby', 'Caversham', 'Chidlow', 'Claremont', 'Clarkson', 'Cloverdale', 'Cockburn Central', 'Collie', 'Como', 'Coolgardie', 'Coolup', 'Cowaramup', 'Cunderdin', 'Currambine', 'Dalwallinu', 'Dampier', 'Dardanup', 'Dawesville', 'Dayton', 'Denmark', 'Derby', 'Dianella', 'Dongara', 'Donnybrook', 'Doubleview', 'Duncraig', 'Dunsborough', 'Dwellingup', 'East Fremantle', 'East Perth', 'East Rockingham', 'East Victoria Park', 'Eaton', 'Edgewater', 'Ellenbrook', 'Embleton', 'Erskine', 'Esperance', 'Exmouth', 'Falcon', 'Fitzroy Crossing', 'Floreat', 'Forrestdale', 'Forrestfield', 'Fremantle', 'Gelorup', 'Geraldton', 'Gidgegannup', 'Girrawheen', 'Glen Forrest', 'Glen Iris', 'Glendalough', 'Glenfield', 'Gnangara', 'Gosnells', 'Gracetown', 'Greenbushes', 'Greenfields', 'Greenough', 'Greenwood', 'Guildford', 'Gwelup', 'Halls Head', 'Hamilton Hill', 'Harrisdale', 'Harvey', 'Henderson', 'Henley Brook', 'Herne Hill', 'High Wycombe', 'Hillarys', 'Huntingdale', 'Innaloo', 'Jandakot', 'Jarrahdale', 'Jindalee', 'Jolimont', 'Joondalup', 'Jurien Bay', 'Kalamunda', 'Kalgoorlie', 'Kambalda East', 'Karawara', 'Kardinya', 'Karnup', 'Karragullen', 'Karratha', 'Karridale', 'Karrinyup', 'Kellerberrin', 'Kelmscott', 'Kewdale', 'Kiara', 'Kingsley', 'Kojonup', 'Koondoola', 'Kununurra', 'Kwinana Beach', 'Kwinana Town Centre', 'Lakelands', 'Landsdale', 'Langford', 'Leda', 'Leederville', 'Leeming', 'Lesmurdie', 'Lexia', 'Lynwood', 'Maddington', 'Madeley', 'Maida Vale', 'Malaga', 'Mandurah', 'Manjimup', 'Manning', 'Manypeaks', 'Margaret River', 'Meadow Springs', 'Meckering', 'Meekatharra', 'Merriwa', 'Middle Swan', 'Midvale', 'Mindarie', 'Mirrabooka', 'Moonyoonooka', 'Moora', 'Morley', 'Mosman Park', 'Mount Barker', 'Mt Helena', 'Mt Lawley', 'Mt Pleasant', 'Mullaloo', 'Mullewa', 'Mundaring', 'Mundijong', 'Munglinup', 'Munster', 'Murdoch', 'Myalup', 'Myaree', 'Narngulu', 'Narrogin', 'Naval Base', 'Nedlands', 'Neerabup', 'Newman', 'Nollamara', 'Noranda', 'Norseman', 'North Bannister', 'North Dandalup', 'North Fremantle', 'North Perth', 'North Yunderup', 'Northam', 'Northbridge', 'Northcliffe', 'Nowergup', 'O Connor', 'Ocean Reef', 'Osborne Park', 'Padbury', 'Palmyra', 'Pearsall', 'Pemberton', 'Perth', 'Picton', 'Picton East', 'Pinjarra', 'Port Hedland', 'Port Kennedy', 'Preston Beach', 'Queens Park', 'Quinns Rocks', 'Ravensthorpe', 'Ravenswood', 'Redcliffe', 'Regans Ford', 'Ridgewood', 'Riverton', 'Rivervale', 'Rockingham', 'Roleystone', 'Rosa Brook', 'Safety Bay', 'Sawyers Valley', 'Scarborough', 'Secret Harbour', 'Serpentine', 'Seville Grove', 'Siesta Park', 'Singleton', 'Sorrento', 'South Fremantle', 'South Hedland', 'South Lake', 'South Perth', 'South Yunderup', 'Southern River', 'Spearwood', 'Stratham', 'Stratton', 'Subiaco', 'Success', 'Swan View', 'Swanbourne', 'Tammin', 'The Lakes', 'Thornlie', 'Tuart Hill', 'Upper Swan', 'Vasse', 'Victoria Park', 'Waikiki', 'Walkaway', 'Walpole', 'Wangara', 'Wanneroo', 'Warnbro', 'Waroona', 'Warwick', 'Waterloo', 'Wattle Grove', 'Wedgefield', 'Wellstead', 'Welshpool', 'Wembley', 'West Busselton', 'West Kalgoorlie', 'West Perth', 'West Pinjarra', 'West Swan', 'Westminster', 'Willetton', 'Williams', 'Witchcliffe', 'Woodbridge', 'Woodvale', 'Wubin', 'Wundowie', 'Yanchep', 'Yangebup', 'Yokine', 'York', 'Youngs Siding'])
    
    
    ***   Surrounding  Filters are   ***
    
    
    dict_keys(['FilterName', 'yes', 'no'])
    
    
    ***   Sort_on  Filters are   ***
    
    
    dict_keys(['Filtername', 'brand', 'location', 'price'])
    
    
    ***   keys_of_interest  Filters are   ***
    
    
    dict_keys(['Filtername', 'title', 'title_detail', 'summary', 'summary_detail', 'brand', 'updated', 'price', 'trading-name', 'location', 'address', 'phone', 'latitude', 'longitude'])
    
    
    
